package com.keyla.appcuentos.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.keyla.appcuentos.R

class PatitoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patito)

        val btnContinuar = findViewById<Button>(R.id.btnContinuar)

        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")

        btnContinuar.setOnClickListener{ goToCuento(nombre,apellido)}
        }
        //botnImagen.setOnClickListener()
    }




    private fun goToCuento(nombre: String?, apellido: String?) {

        val intent = Intent(this, CuentoActivity::class.java)
        intent.putExtra("dato1", nombre)
        intent.putExtra("dato2", apellido)
        //intent.putExtra("dato3", "El Patito Feo")

        startActivity(intent)
    }
}

