package com.keyla.appcuentos.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.keyla.appcuentos.R

class ActivityCerdito5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cerdito5)
        val btnContinuar = findViewById<Button>(R.id.btnContinuar)

        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")

            btnContinuar.setOnClickListener { goToCerdito3(nombre,apellido) }
        }
    }
    private fun goToCerdito3(nombre : String?, apellido : String?){

        val intent = Intent(this,FinalCuentoActivity::class.java)
        intent.putExtra("dato1",nombre)
        intent.putExtra("dato2",apellido)
        startActivity(intent)
    }
}