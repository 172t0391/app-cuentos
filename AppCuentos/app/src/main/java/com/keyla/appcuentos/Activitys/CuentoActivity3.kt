package com.keyla.appcuentos.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.keyla.appcuentos.R
import kotlinx.android.synthetic.main.activity_cuento.*

class CuentoActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento_activiy3)
        val btnContinuar = findViewById<Button>(R.id.btnContinuar)

        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")
            val cuento = it.getString("dato3")
           // txtLector.text = "$nombre estas leyendo: $cuento "

            btnContinuar.setOnClickListener { goToCuentoParte3(nombre,apellido) }

        }

    }
        private fun goToCuentoParte3(nombre : String?, apellido : String?){

        val intent = Intent(this,CuentoActivity4::class.java)
            intent.putExtra("dato1",nombre)
            intent.putExtra("dato2",apellido)
        startActivity(intent)
    }
}