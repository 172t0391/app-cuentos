package com.keyla.appcuentos.Activitys

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.keyla.appcuentos.R
import kotlinx.android.synthetic.main.activity_cuento.*

class CuentoActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento)

      val btnSi = findViewById<Button>(R.id.btnSi) //Declaracion de los componentes en tu activity
      val btnNo = findViewById<Button>(R.id.btnNo)

        val bundle: Bundle? = intent.extras //Comienzas a recuperar los datos
        bundle?.let {

            val nombre = it.getString("dato1") //Recupera los datos del formulario
            val apellido = it.getString("dato2") //
            //val edad = it.getString("dato3")
            txtLector.text = "$nombre $apellido estas leyendo El Patito Feo" //Mostrando el dato en el layout
           // txtPregunta.text = "$nombre $apellido "

            btnSi.setOnClickListener { goToCuento(nombre,apellido) }//Este es el evento del boton, dentro de el puedes enviar los datos o hacer una
            //funcion, como las de abajo
            btnNo.setOnClickListener { goToFinal(nombre,apellido) }
        }

    }

    private fun goToCuento(nombre: String?, apellido : String?){
        val intent = Intent(this,ActivityCuento2::class.java)

        intent.putExtra("dato1",nombre) //Enviando datos a otro activity
        intent.putExtra("dato2",apellido)
        intent.putExtra("dato3", "El Patito Feo")
        startActivity(intent)
        finish()


    }
    private fun goToFinal(nombre: String?,apellido: String?){

        val intent = Intent(this,FinalCuentoActivity::class.java)

        intent.putExtra("dato1",nombre) //Enviando datos a otro activyty
        intent.putExtra("dato2",apellido)
        intent.putExtra("dato3", "El Patito Feo")
        startActivity(intent)
        finish()

    }
}